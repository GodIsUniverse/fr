import { TestBed, inject } from '@angular/core/testing';

import { LoaderStore } from './loader-store';

describe('LoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoaderStore]
    });
  });

  it('should be created', inject([LoaderStore], (service: LoaderStore) => {
    expect(service).toBeTruthy();
  }));
});
