import { TestBed, inject } from '@angular/core/testing';

import { ArViewerStore } from './ar-viewer-store';

describe('ArViewerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArViewerStore]
    });
  });

  it('should be created', inject([ArViewerStore], (service: ArViewerStore) => {
    expect(service).toBeTruthy();
  }));
});
