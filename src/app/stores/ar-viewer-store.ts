import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable, PartialObserver} from 'rxjs';
import { map, filter, distinctUntilChanged, distinctUntilKeyChanged } from 'rxjs/operators';
import { ThreeScene } from '../ar-viewer/ar-viewer.component';
import { LoaderStore } from './loader-store';
import { ModelName } from '../enums/models3d';
declare const THREE: any;

export class ArViewerState {
    arVisible: boolean;
    currentlyPickedModelName: ModelName;   
    mainScene: ThreeScene;
}

@Injectable({
    providedIn: 'root'
})
export class ArViewerStore {    
    public actions: ArViewerStoreActions = new ArViewerStoreActions();
    private _state$: BehaviorSubject<ArViewerState> = new BehaviorSubject<ArViewerState>({arVisible: true, currentlyPickedModelName: null, mainScene: new THREE.Scene()});
    public state$: Observable<ArViewerState>;    

    get state (): ArViewerState {
        return this._state$.getValue();
    }

    constructor(public readonly loaderStore: LoaderStore) {

        this.registerHandlers();
        this.registerChanges();
        this.state$ = this._state$ as Observable<ArViewerState>;
    }

    private registerHandlers() {
        this.actions.setArVisibility.subscribe(visibility => {
            this._state$.next({...this.state, arVisible: visibility});
        });        

        this.actions.setArModelName.subscribe((modelName) => {                
            this._state$.next({arVisible: true, currentlyPickedModelName: modelName, mainScene: this.state.mainScene});
        });
    }

    public registerChanges() {
        this._state$.pipe(distinctUntilKeyChanged("currentlyPickedModelName"), map(x=> x.currentlyPickedModelName), filter(x=>x != null)).subscribe(
            modelName => {
                this.loaderStore.actions$.initiateModelLoading.next({modelName: modelName, scene: this.state.mainScene})
            }
        );
    }
}

export class ArViewerStoreActions {
    setArVisibility: Subject<boolean> = new Subject<boolean>();
    setArModelName: Subject<ModelName> = new Subject<ModelName>();
}