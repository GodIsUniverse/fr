import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilKeyChanged, filter } from 'rxjs/operators';
import { MappingDictionary } from '../ar-viewer/models3d-path-mapping';
import { ModelName, ModelPath, ModelType } from '../enums/models3d';
declare const THREE:any;
@Injectable({
  providedIn: 'root'
})
export class LoaderStore {    
    public actions$: LoaderStoreActions = new LoaderStoreActions();   
    public changes$: LoaderStoreChanges = new LoaderStoreChanges();  
    public state$: Observable<LoaderState>;    

    private modelsPathMappingDictionary = new MappingDictionary<ModelName, ModelPath, ModelType>();    
    private gltfLoader: Loader = new THREE.GLTFLoader();
    private _state$: BehaviorSubject<LoaderState> = new BehaviorSubject<LoaderState>(
        {modelsCache: [], currentModel: null, isLoading: false, loadingProgress: null}
    );    

    get state (): LoaderState {
        return this._state$.getValue();
    }

    constructor() {        
        this.modelsPathMappingDictionary.add({key: ModelName.DanieDispersed, value: ModelPath.DanieDispersed, type: ModelType.GLTF});
        this.modelsPathMappingDictionary.add({key: ModelName.Sushi, value: ModelPath.Sushi, type: ModelType.GLTF});
        
        this.registerHandlers();
        this.registerStateChanges();
        this.state$ = this._state$.asObservable();
    }
    
    registerHandlers() {
        this.actions$.initiateModelLoading.subscribe(({modelName, scene}) => {  
            this._state$.next({...this.state, isLoading: true}); 
            
            const ctrl = this;
            const modelPath = this.modelsPathMappingDictionary.getValueByKey(modelName); 
            const loader = this.resolveLoaderByModelType(this.modelsPathMappingDictionary.getTypeByKey(modelName))
            const positionOfModelInCache = this.state.modelsCache.findIndex(x=>x.name === modelName);

            if (positionOfModelInCache >= 0) {            
                console.log(this.state.modelsCache[positionOfModelInCache]);                   
                this._state$.next({...this.state, currentModel: this.state.modelsCache[positionOfModelInCache], isLoading: false});  
                return;         
            }   
              
            const onLoad = function ( gltf ) {
                console.log(gltf);
                gltf.scene.scale.set(0.45, 0.45, 0.45);
                gltf.scene.position.set(0, 0, 0);
                    setTimeout(() => {
                        scene.add( gltf.scene );
                    }, 0);                    
                ctrl.cacheModelAndSetLoadingToFalse(new Model3d(modelName, gltf.scene));
            };

            const onProgress = function (progressXML) {
                console.log(progressXML + modelPath);
                const percentage = Math.round(progressXML.loaded/progressXML.total * 100);
                // ctrl.actions$.progressingWithLoad.next(percentage);
                ctrl._state$.next({...ctrl.state, loadingProgress: percentage}); 
            };

            const onError = function ( error ) {
                console.error( error );
                ctrl._state$.next({...ctrl.state, isLoading: false});
            };

            loader.load( modelPath, onLoad, onProgress, onError);
                   
        });
    }

    private registerStateChanges() {

        this._state$.pipe(
            distinctUntilKeyChanged("currentModel"), filter(x=>x.currentModel != null)
        ).subscribe( x=> {
            this.changes$.currentModelChanged.next(x.currentModel);             
             console.log("stateLoadingChanged:" + x);
        });
    }

    private cacheModelAndSetLoadingToFalse(model: Model3d) {
        this.state.modelsCache.push(model);    
        this._state$.next({...this.state, modelsCache: this.state.modelsCache, currentModel: model, isLoading: false});            
    }

    private resolveLoaderByModelType(type: ModelType) {
        switch (type) {
            case ModelType.GLTF:
            return this.gltfLoader;
        }        
    }
}

export class LoaderStoreActions {
    initiateModelLoading = new Subject<{modelName: ModelName, scene: any}>();    
    finishLoadingSuccessfully = new Subject<Model3d>();
    progressingWithLoad = new Subject<number>();
}

export class LoaderStoreChanges {
    loadingStarted = new Subject();
    loadingFinishedSuccessfully = new Subject<Model3d>();
    currentModelChanged = new Subject<any>();
}

export class LoaderState {    
    modelsCache: Model3d[];
    currentModel: Model3d;
    isLoading: boolean;    
    loadingProgress: number;
}

export class Model3d {    
    constructor(public name: string = null, public model: any = null){
    }
}

export class Loader {
    load: Function;
}