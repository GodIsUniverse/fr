import { Directive, Input, Output, EventEmitter, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[longPress]'
})
export class LongPressDirective {

    @HostBinding('style.opacity')
    opacity: number = 0.5;
//   constructor() { }
    @Input() longPressAfterMs: number = 500;

    @Output() onPress: EventEmitter<any> = new EventEmitter();
    @Output() onHold: EventEmitter<any> = new EventEmitter();
    @Output() onLongPressEnd: EventEmitter<any> = new EventEmitter();

    isPressed: boolean = false;
    isHolding: boolean = false;
    isOverElement: boolean = false;    
    timeoutLoop: any;
    timeoutInitial: any;

    @HostListener("mousedown", ['$event']) 
    @HostListener("touchstart", ['$event']) 
    onMouseDown(event){
        this.onPress.emit();        
        this.isOverElement = true;
        this.timeoutInitial = setTimeout(()=> {this.isHolding = true; this.loop(event)}, this.longPressAfterMs);
    }

    loop(event: any) {
        if(this.isOverElement === false || this.isHolding === false)
        {
            this.breakLoop();
        } else {
            this.timeoutLoop = setTimeout(()=>{
                this.opacity = 1;
                this.onHold.emit();
                this.loop(event);            
            }, 50);
        }
    }

    @HostListener("mouseup", ['$event'])    
    @HostListener("touchend", ['$event']) 
    onMouseUp(event) {
        clearTimeout(this.timeoutInitial);
        this.isHolding = false;
    }

    @HostListener("mouseleave", ['$event']) 
    @HostListener("touchcancel", ['$event'])
    onMouseLeave(event){
        clearTimeout(this.timeoutInitial);
        this.isOverElement = false;        
    }

    breakLoop(){        
        clearTimeout(this.timeoutInitial);
        clearTimeout(this.timeoutLoop);
        this.opacity = 0.5;
        this.onLongPressEnd.emit();
    }
}
