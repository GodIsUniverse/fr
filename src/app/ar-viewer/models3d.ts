
declare const THREE: any;
import { ArViewerStore } from "../stores/ar-viewer-store";

export class Models3D {
    _onRenderFcts: any;
    constructor(public readonly arViewerStore: ArViewerStore , onRenderFcts: any) {
        this._onRenderFcts = onRenderFcts;
    }

    public plane() {
        var geo = new THREE.PlaneBufferGeometry(1.5, 1.5, 8, 8);
        var mat = new THREE.MeshBasicMaterial({ color: 0x228B22, side: THREE.DoubleSide });
        var plane = new THREE.Mesh(geo, mat);
        plane.rotateX( - Math.PI / 2);
        this.arViewerStore.state.mainScene.add(plane);
    }

    public cubeAnimated() {
        const geometry2 = new THREE.BoxGeometry(0.1, 0.1, 0.1);
        const material = new THREE.MeshNormalMaterial({
            transparent: true,
            opacity: 0.3,
            side: THREE.DoubleSide
        });
        const mesh2 = new THREE.Mesh(geometry2, material);
        mesh2.position.y = geometry2.parameters.height / 2;
        mesh2.position.x = 1;
        this.arViewerStore.state.mainScene.add(mesh2);
        this._onRenderFcts.push(function(delta){
            mesh2.rotation.y -= Math.PI*delta
        })
    }
}