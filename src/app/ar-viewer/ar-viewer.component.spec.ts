import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArViewerComponent } from './ar-viewer.component';

describe('ArViewerComponent', () => {
  let component: ArViewerComponent;
  let fixture: ComponentFixture<ArViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
