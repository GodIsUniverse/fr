export class MappingDictionary<TK, TV, TT> {
    dictionary: KeyValueTypeObject<TK, TV, TT>[];

    constructor() {
        this.dictionary = [];
    }
    
    public add(keyValueTypeObject: KeyValueTypeObject<TK, TV, TT>) {
        this.dictionary.push(keyValueTypeObject);
    }

    public getValueByKey(key: TK) {
        const pair = this.dictionary.find(element => element.key === key);
        return pair ? pair.value : null;
    }

    public getTypeByKey(key: TK) {
        const pair = this.dictionary.find(element => element.key === key);
        return pair ? pair.type : null;
    }
}

export class KeyValueTypeObject<TK, TV, TT> {
    key: TK;
    value: TV;
    type: TT;
}