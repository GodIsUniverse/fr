export class Mesh implements Object {
    position: {
        x: number,
        y: number,
        z: number
    };
    rotation: {
        x: number,
        y: number,
        z: number
    }
    scale: {
        set: (x,y,z)=> void
    };
    clone: Function;
    rotateX: Function;
    name: any;
}
