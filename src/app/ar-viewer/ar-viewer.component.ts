import { Component, OnInit } from '@angular/core';
import { ArViewerStore } from "../stores/ar-viewer-store";
import { Models3D } from './models3d';
import { LoaderStore, Model3d } from '../stores/loader-store';
import { Mesh } from './models/mesh';
import { ModelName } from '../enums/models3d';
import { distinctUntilChanged, map, distinctUntilKeyChanged } from '../../../node_modules/rxjs/operators';
declare const THREEx: any;
declare const THREE: any;

@Component({
  selector: 'app-ar-viewer',
  templateUrl: './ar-viewer.component.html',
  styleUrls: ['./ar-viewer.component.scss']
})
export class ArViewerComponent implements OnInit {
    public renderer: any;
    public onRenderFcts: Array<Function> = [];
    public camera: any;
    public mainScene: ThreeScene;
    public arToolkitSource: any;
    public arToolkitContext: any;
    public controls: any;
    public currentlySelectedModel: Model3d;
    public models: Models3D;
    public loadingProgress: number;
    public isArVisible = true;
    public angle = 0;
    public radius = 0; 

    constructor(private arViewerStore: ArViewerStore, private loaderStore: LoaderStore) {
        
    }

    private registerHandlers() {        
        const ctrl = this;
        this.arViewerStore.state$.pipe(distinctUntilChanged((x,y) => x.arVisible === y.arVisible), map(x=>x.arVisible)).subscribe(value => 
        {
            this.isArVisible = value;
            const isVisibleString = value ? 'visible' : 'hidden';
            this.renderer.domElement.style.visibility = isVisibleString;
            this.arToolkitSource.domElement.style.visibility = isVisibleString;
        })

        this.arViewerStore.state$.pipe(distinctUntilKeyChanged("currentlyPickedModelName"), map(x=> x.currentlyPickedModelName)).subscribe(() => 
        {                
            this.cleanUpScene();
        })
        
        this.loaderStore.changes$.currentModelChanged.subscribe(currentModel=> {    
            this.currentlySelectedModel = currentModel; 
            ctrl.mainScene.add(currentModel.model);        
        });
    }

    ngOnInit() {
        const ctrl: ArViewerComponent = this;  
        this.mainScene = this.arViewerStore.state.mainScene;
        window.oncontextmenu = function(event) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        };
      
        this.initializeRenderer(); 
        this.initializeSceneAndCamera();
        this.initializeArToolkitContext();
        this.handleArToolkitSource();
        this.updateArToolKitOnFrameFct(ctrl);
        this.initControlsForCamera();
        
        this.registerHandlers();
        // this.models = new Models3D(this.arViewerStore, this.onRenderFcts);     
        this.renderCameraFct(ctrl);
        this.loaderStore.actions$.initiateModelLoading.next({modelName: ModelName.DanieDispersed, scene: this.mainScene});
        this.runRenderingLoop(ctrl);
    }

    protected goBackToMenu() {
        this.arViewerStore.actions.setArVisibility.next(false);
    }

    protected rotateScene(counterclockwise: boolean = false) {
        this.currentlySelectedModel.model.rotation.y += counterclockwise ? 0.1 : -0.1;
    }

    // Rendering methods
    private runRenderingLoop(ctrl: ArViewerComponent) {
        var lastTimeMsec = null;
        requestAnimationFrame(function animate(nowMsec) {
            // keep looping           
            requestAnimationFrame(animate);
            // measure time
            lastTimeMsec = lastTimeMsec || nowMsec - 1000 / 60;
            var deltaMsec = Math.min(200, nowMsec - lastTimeMsec);
            lastTimeMsec = nowMsec;
            // call each update function
            ctrl.onRenderFcts.forEach(function (onRenderFct) {
                onRenderFct(deltaMsec / 1000, nowMsec / 1000);
            });
        });
    }

    private renderCameraFct(ctrl: ArViewerComponent) {        
        this.onRenderFcts.push(function () {
            ctrl.renderer.render(ctrl.mainScene, ctrl.camera);
        });
    }

    private initControlsForCamera() {
        var markerControls = new THREEx.ArMarkerControls(this.arToolkitContext, this.camera, {
            type: 'pattern',
            patternUrl: THREEx.ArToolkitContext.baseURL + './data/Markers/FMR-marker20.patt',
            changeMatrixMode: 'cameraTransformMatrix'
        });
        this.mainScene.visible = false;
        console.log(THREEx.ArToolkitContext.baseURL);
        console.log(THREEx.ArToolkitContext.baseURL + './data/Markers/your-company-pattern-marker.patt');
    }

    private updateArToolKitOnFrameFct(ctrl: ArViewerComponent) {
        this.onRenderFcts.push(function () {
            if (ctrl.arToolkitSource.ready === false) {
                return;
            };
            ctrl.mainScene.visible = ctrl.camera.visible;
            ctrl.arToolkitContext.update(ctrl.arToolkitSource.domElement);
        });
    }

    private initializeArToolkitContext() {
        let ctrl = this;
        this.arToolkitContext = new THREEx.ArToolkitContext({
            cameraParametersUrl: THREEx.ArToolkitContext.baseURL + './data/camera_para.dat',
            detectionMode: 'mono',
            patternRatio: 0.7,
            maxDetectionRate: 30,
            canvasWidth: 80*3,
            canvasHeight: 60*3,
        });
        this.arToolkitContext.init(function onCompleted() {
            ctrl.camera.projectionMatrix.copy(ctrl.arToolkitContext.getProjectionMatrix());
        });
    }

    // TODO: Check if it's valid to inject renderer elsewhere and if re-instantiating renderer via routing is not too slow
    private initializeRenderer(): void {
        this.renderer	= new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });
        this.renderer.setClearColor(new THREE.Color('white'), 0);
        // this.renderer.setSize( window.innerWidth, window.innerHeight );
        this.renderer.setSize( 800, 600 );
        this.renderer.domElement.style.position = 'absolute';
        this.renderer.domElement.style.top = '0px';
        this.renderer.domElement.style.left = '0px';        
        this.renderer.domElement.style.zIndex = '-1';   
        document.body.appendChild( this.renderer.domElement );        
    }

    private initializeSceneAndCamera(): void {
        this.mainScene	= this.arViewerStore.state.mainScene;
        let ambientLight = new THREE.AmbientLight( 0xffffff,1 );
        this.mainScene.add( ambientLight );  
		// 			let lightNode = new THREE.SpotLight( 0xffffff, 1.5 );
		// 			lightNode.position.set( 2, 2, 2 );
        // this.scene.add( lightNode );          
        this.camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 10000 );
        this.mainScene.add(this.camera);    
    }

    private handleArToolkitSource() {
        let ctrl = this;
        this.arToolkitSource = new THREEx.ArToolkitSource({sourceType : 'webcam'});

        this.arToolkitSource.init(function onReady(){
            onResize();                    
        });

        window.addEventListener('resize', function(){
            onResize();
        });
        
        function onResize(){
            ctrl.arToolkitSource.onResize()	;
            ctrl.arToolkitSource.copySizeTo(ctrl.renderer.domElement);
            ctrl.arToolkitSource.domElement.zIndex = -1;
            if( ctrl.arToolkitContext.arController !== null ){
                ctrl.arToolkitSource.copySizeTo(ctrl.arToolkitContext.arController.canvas)	;
            }	
        }        
    }

    private cleanUpScene() {
        // omit light and camera
        while(this.mainScene.children.length > 2){             
            this.mainScene.remove(this.mainScene.children[this.mainScene.children.length - 1]); 
        }

        // var selectedObject = this.mainScene.getObjectByName(this.currentModelName);
        // this.mainScene.remove( selectedObject );
    }   
}

export class ThreeScene extends Mesh {
    children: any;
    getObjectByName: Function;
    add: Function;
    remove: Function;    
    visible: boolean;
}