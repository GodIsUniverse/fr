export enum ModelName{
    DanieDispersed = "DanieDispersed",
    Sushi = "Sushi"
}

export enum ModelPath{
    DanieDispersed = './assets/data/Models/gltf/DanieDispersedNew6.gltf',
    Sushi = './assets/data/Models/gltf/sushiDispersed3.gltf'
}

export enum ModelType{
    GLTF = "GLTF"    
}