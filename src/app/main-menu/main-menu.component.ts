import { Component, OnInit } from '@angular/core';
import { ArViewerStore } from "../stores/ar-viewer-store";
import { ModelName } from '../enums/models3d';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {    
  constructor(private arViewerStore: ArViewerStore) { }

  foodGroups: Category[] = [];

  ngOnInit() {
    this.foodGroups.push({title: "MAIN DISHES", elements: [ModelName.DanieDispersed, ModelName.Sushi]});
    this.foodGroups.push({title: "SOUPS", elements: ["Schaboszczak", "Kurak"]});
    this.foodGroups.push({title: "PIZZA", elements: ["Karpiczioza", "Polonez", "Mazurek Dąbrowskiego"]});
    this.foodGroups.push({title: "PASTA", elements: ["Uszka", "Noski"]});
    this.foodGroups.push({title: "DESSERT", elements: ["Ciacho", "Baton"]});
  }  

  navigate(value: ModelName) {
      this.arViewerStore.actions.setArModelName.next(value);
  }
}

class Category {
    title: string;
    elements: string[];
}
